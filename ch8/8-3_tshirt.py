def make_shirt(shirt_size, shirt_message):
    print("I like your " + shirt_size + ' '+ shirt_message + " shirt.")


# defaults.

# call doing positional
make_shirt('large', 'volcom')
# call using keywords
make_shirt(shirt_size='large', shirt_message='volcom')


# shirt size is large by default.
#8-4
def make_shirt(shirt_message='I love Python.', shirt_size='large'):
    #shirt_size = 'large'
    print("I like your " + shirt_size + ' '+ shirt_message)