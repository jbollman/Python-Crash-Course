import sys
import pygame

from settings import Settings
from ship import Ship
import game_functions as game_functions


def run_game():
    pygame.init()
    # Initalizing a Settings class object.
    ai_settings = Settings()
    screen = pygame.display.set_mode(
        (ai_settings.screen_width, ai_settings.screen_height))
    pygame.display.set_caption("Alien Invasion")

    # Make a ship.
    ship = Ship(screen)

    # Set the background color.
    background_color = (230, 230, 230)
    # Starts game loop
    while True:
        # Starts the event loop
        game_functions.check_events(ship)
        game_functions.update_screen(ai_settings, screen, ship)
        # Redraw the screen during each pass through the loop.
        screen.fill(background_color)
        ship.blitme()
        # Make the most recently drawn screen visible.
        pygame.display.flip()


run_game()

