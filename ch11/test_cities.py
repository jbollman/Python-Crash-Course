import unittest

import city_functions

class CityTest(unittest.TestCase):
    def test_city_country(self):
        self.assertEqual(city_functions.city_country("baltimore", "USA"), "Baltimore, Usa")

    def test_city_country_population(self):
        self.assertEqual(city_functions.city_country("Paris", "France", 1000000), "Paris, France - population 1000000")


if __name__ == '__main__':
    unittest.main()
