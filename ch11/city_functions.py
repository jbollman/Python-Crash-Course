def city_country(city, country, population=""):
    """Function takes two inputs and outputs a string"""
    if population:
        output = city + "," + " " + country + " - "
        return output.title() + 'population ' + str(population)
    else:
        output = city + "," + " " + country
        return output.title()
