class User():
    """A class to create a user with basic properties."""
    def __init__(self, first_name, last_name, username, email):
        """Initalizes class attributes"""
        self.first_name = first_name
        self.last_name = last_name
        self.username = username
        self.email = email

    def describe_user(self):
        print("first name == " + self.first_name + "\r\n last name == " + self.last_name + "\r\n username == " + self.username + "\r\n email == " + self.email)
    def greet_user(self):
        print('Welcome ' + self.first_name.title())

test_user = User('henry', 'bollman', 'hbollman', 'hbollman@gmail.com')
test_user.describe_user()
test_user.greet_user()