from first_exc_restaurants import *


restaurant1 = Restaurant('jojos', 'corndogs', 'closed' )
restaurant2 = Restaurant('jbs', 'grilled-cheese', 'open')
restaurant3 = Restaurant('fatgirl', 'gravy-emporium', 'closed')

restaurant1.describe_restaurant()
restaurant2.describe_restaurant()
restaurant3.describe_restaurant()

