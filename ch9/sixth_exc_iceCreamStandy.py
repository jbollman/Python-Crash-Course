# practice making an inherited class
# Icecreamstand class will inherit from restaurant
# Icecreamstand is a more specific version of Restaurant
# threfore IcecreamSTand extends Restaurant by inheriting form it.
class Restaurant():
    """This class is an example of a very basic restaurant class type"""
    def __init__(self, name, cusine, status):
        """Initializes class attributes"""
        self.restaurant_name = name
        self.cusine_type = cusine
        self.status = status
    def describe_restaurant(self):
        print('This restaurant names is ' + str(self.restaurant_name.title()) + ' and it serves ' + str(self.cusine_type) + ' cusine!')
    def open_restaurant(self):
        if self.status == 'open':
            print(str(self.restaurant_name.title()) + ' is ' + str(self.status))
        else:
            print(str(self.restaurant_name.title) + ' is ' + str(self.status))
    
"""    
def main():
    restaurant = Restaurant('boby ds', 'waffle house', 'open')
    print(restaurant.cusine_type)
    print(restaurant.restaurant_name)
    print(restaurant.status)
    print(restaurant.describe_restaurant())
    print(restaurant.open_restaurant())


if __name__ == '__main__':
  main()
"""





class IceCreamStand(Restaurant):
    """Represent aspects of a car, specific to electric vehicles."""
    def __init__(self, name, cusine, status, flavors):
        """Initialize attributes of the parent class."""
        super().__init__(name, cusine, status)
        self.flavors = flavors

    def display_flavors(self):
        print("Several flavors to choose from")
        for _ in self.flavors:
            print("The specials today are " + _ + ".")





def main():
    flavors = ['chocolate', 'strawberry', 'vanilla']
    ice_cream = IceCreamStand('jojos', 'icecream', 'open', flavors)
    ice_cream.display_flavors()


if __name__ == '__main__':
  main()

