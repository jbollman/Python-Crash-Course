class Restaurant():
    """This class is an example of a very basic restaurant class type"""
    def __init__(self, name, cusine, status):
        """Initializes class attributes"""
        self.restaurant_name = name
        self.cusine_type = cusine
        self.status = status
        self.number_served = 0
    def describe_restaurant(self):
        print('This restaurant names is ' + str(self.restaurant_name.title()) + ' and it serves ' + str(self.cusine_type) + ' cusine!')
    def open_restaurant(self):
        if self.status == 'open':
            print(str(self.restaurant_name.title()) + ' is ' + str(self.status))
        else:
            print(str(self.restaurant_name.title) + ' is ' + str(self.status))
    def set_numbers_served(self, number):
        self.number_served = number
    def increment_number_served(self, value_to_increment_by):
        self.number_served += value_to_increment_by
    
    
def main():
    restaurant = Restaurant('boby ds', 'waffle house', 'open')
    print(restaurant.number_served)
    restaurant.number_served = 7
    print(restaurant.number_served)

    print(restaurant.set_numbers_served(41))
    print(restaurant.number_served)

if __name__ == '__main__':
  main()