from eighth_exc_privileges import Privileges

class User():
    """A class to create a user with basic properties."""
    def __init__(self, first_name, last_name, username, email):
        """Initalizes class attributes"""
        self.first_name = first_name
        self.last_name = last_name
        self.username = username
        self.email = email
        self.login_attempts = 0
    def describe_user(self):
        print("first name == " + self.first_name + "\r\n last name == " + self.last_name + "\r\n username == " + self.username + "\r\n email == " + self.email)
    def greet_user(self):
        print('Welcome ' + self.first_name.title())
    def increment_login_attempts(self):
        self.login_attempts +=1
    def reset_login_attempts(self):
        self.login_attempts = 0

"""
test_user = User('henry', 'bollman', 'hbollman', 'hbollman@gmail.com')
for element in range(5):
    test_user.increment_login_attempts()

print(test_user.login_attempts)
test_user.reset_login_attempts()
print(test_user.login_attempts)
"""


class Admin(User):
    """Admin extends the User class."""
    def __init__(self, first_name, last_name, username, email):
        super().__init__(first_name, last_name, username, email)
        #self.privileges = ['can add post', 'can delete post', 'can ban user']
        self.privilege = Privileges()



admin_test = Admin('hank', 'aaron', 'harron', 'harron@baseball.net')
admin_test.privilege.show_privileges()