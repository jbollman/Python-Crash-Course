class Privileges():
    def __init__(self, privileges = []):
        self.privileges = ['can add post', 'can delete post', 'can ban user']

    def show_privileges(self):
        for _ in self.privileges:
            print (_)


class User():
    """A class to create a user with basic properties."""
    def __init__(self, first_name, last_name, username, email):
        """Initalizes class attributes"""
        self.first_name = first_name
        self.last_name = last_name
        self.username = username
        self.email = email
        self.login_attempts = 0
    def describe_user(self):
        print("first name == " + self.first_name + "\r\n last name == " + self.last_name + "\r\n username == " + self.username + "\r\n email == " + self.email)
    def greet_user(self):
        print('Welcome ' + self.first_name.title())
    def increment_login_attempts(self):
        self.login_attempts +=1
    def reset_login_attempts(self):
        self.login_attempts = 0



class Admin(User):
    """Admin extends the User class."""
    def __init__(self, first_name, last_name, username, email):
        super().__init__(first_name, last_name, username, email)
        #self.privileges = ['can add post', 'can delete post', 'can ban user']
        self.privilege = Privileges()

