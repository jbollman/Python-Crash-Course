class Restaurant():
    """This class is an example of a very basic restaurant class type"""
    def __init__(self, name, cusine, status):
        """Initializes class attributes"""
        self.restaurant_name = name
        self.cusine_type = cusine
        self.status = status
    def describe_restaurant(self):
        print('This restaurant names is ' + str(self.restaurant_name.title()) + ' and it serves ' + str(self.cusine_type) + ' cusine!')
    def open_restaurant(self):
        if self.status == 'open':
            print(str(self.restaurant_name.title()) + ' is ' + str(self.status))
        else:
            print(str(self.restaurant_name.title) + ' is ' + str(self.status))
    
    
def main():
    restaurant = Restaurant('boby ds', 'waffle house', 'open')
    print(restaurant.cusine_type)
    print(restaurant.restaurant_name)
    print(restaurant.status)
    print(restaurant.describe_restaurant())
    print(restaurant.open_restaurant())


if __name__ == '__main__':
  main()