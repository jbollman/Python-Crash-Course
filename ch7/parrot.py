prompt = "\nTell me anything, and I will repeat it back to you."
prompt += "\n Enter 'quit' to end the program. "

message = ''
while message != 'quit':
    message = input(prompt)
    if message != 'quit':
        print("\n" + message)


    