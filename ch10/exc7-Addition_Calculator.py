# Wrap you code from exc 10-6 in a while loop so that a user
# cna continue entering numbers iven if they make a mistake
# and enter text instead of a number

balance = 0
while True:
    num1 = input('enter first number ')
    num2 = input('enter second number ')
    try:
        addition = int(num1) + int(num2)
        balance += addition
    except ValueError:
        pass
    else:
        #print("{} + {} equals" +  str(addition) + ' with a balance of ' + str(balance)).format(num1, num2))
        print("With a starting balance of {}; {} + {} equals ".format(balance, num1,num2) 
              + 'with a balnce of ' + str(balance))


