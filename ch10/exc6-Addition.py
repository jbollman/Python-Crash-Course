# the input to an int, you'll get a ValueError.  Write a program
# that prompts for two nubmers. Add them together and print the result.
# catch the ValueError if either input value is not a number, and print a friendly
# Error message
#Test your program by entering two numbers and them by entering some text instead
# of a number



def addition(arg1, arg2):
    try:
        add_numbers = int(arg1) + int(arg2)
    except ValueError:
        print('you must pass a number')
        print('example 5 or "66" with quotations')
    else: 
        print(add_numbers)
