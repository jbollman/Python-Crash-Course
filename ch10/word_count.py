def count_words(filename):
    """ Count the words in  a file."""
    try:
        with open(filename) as f_obj:
            contents = f_obj.read()
    except FileNotFoundError:
        msg = "Sorry, the file "  + filename + " does not exist."
        print(msg)
    else:
        # count words
        words = contents.split()
        num_words = len(words)
        print("The file " + filename + " has " + str(num_words) + " words.")

filename = "programming.txt"
count_words(filename)
