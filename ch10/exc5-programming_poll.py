# Write a while loop that asks people why they like programming. Each time someone enters a reason, add their reason to afile that saves a response.


filename = 'programming_poll.txt'

while True:
    polley = input('Why do you like programming? press("q") to quit: ')
    if polley in 'q':
        break
    print('Response added to programming poll file!')
    with open(filename, 'a') as file_object:
        file_object.write(polley +'\n')

