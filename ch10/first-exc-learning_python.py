filename = 'learning_python.txt'
# read entire file
with open(filename) as pfile_object:
    content = pfile_object.read()
    print(content)

# once for looping over file object
with open(filename) as file_object:
    for line in file_object:
        print(line)



# storing the line ina list and then working with them outside the with block
with open(filename) as file_object:
    lines = file_object.readlines()


for line in lines:
    print(line.rstrip())








# Print three different ways
#1 once by reading the entire file
#2 once for looping over file object
#3 storing the line in a list and  then working with them outside the with block
