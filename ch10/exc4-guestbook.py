# Write a while loop that prompts users for their name. Whenthey enter their name, print a greeting to the screen and add a line recording their visit in a file called guest_
#book.txt . Make sure each entry appears as anew line in the file.



filename = 'guest_book.txt'


while True:
    guest = input('What is your name? press("q") to quit: ')
    if guest in 'q':
        break
    print('Welcome to the party! ' + guest)
    with open(filename, 'a') as file_object:
        file_object.write(guest + "\n")


