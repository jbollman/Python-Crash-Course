my_filename = 'programming.txt'

with open(my_filename, 'a') as file_object:
    file_object.write("I also love finding meaning in large datasets.\n")
    file_object.write("I love creating apps that people find useful.\n")
