# make two files, cats.txt and dogs.txt
# store at least three names of cats in the first file and three
# names of dogs in second file.
# Write a program to read these files and print the contents
# of the file to the screen. Wrap your code in try except block

# set up cat and dog files
cat_file = 'cats.txt'
dog_file = 'dogs.txt'

cat_names = ['Trixy', 'mittens', 'Mr. Whiskers']
dog_names = ['Duke', 'Jack', 'Rex']

for cat in cat_names:
    with open(cat_file, 'a') as f_object:
        f_object.write(cat + ', ')


for dog in dog_names:
    with open(dog_file, 'a') as f_object:
        f_object.write(dog + ', ')

# read and print conents of file
try:
    with open(cat_file) as f_object:

except FileNotFound:
    pass
else:
    for line in f_object:
        print(line.rstrip())

try:
    with open(dog_file) as f_object:
        for line in f_object:
            print(line.rstrip())
except FileNotFound:
    pass
