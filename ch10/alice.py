# use Try Except to handle filenotfounderror



filename = 'alice.txt'

try:
    with open(filename) as file_object:
        contents = file_object.read()
except FileNotFoundError:
    print('yo that {} file doesnt exist'.format(filename))

else:
    print(contents)
