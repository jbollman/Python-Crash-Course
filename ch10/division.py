print("give two numers for division operation.")
print("Enter 'q' to quit/")

while True:
    first_number = input("\nFirst number: ")
    second_number = input("Second number: ")
    if (first_number or  second_number) in 'q':
        break
    
    try:
        answer = int(first_number) / int(second_number)
    except ZeroDivisionError:
        print('you cant divide by 0!')
    else:
        print(answer)
