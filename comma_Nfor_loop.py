favorite_languages = {
    'joe': ['python', 'java'],
    'toan': ['python', 'perl'],
    'larry': ['python'],
    'aaron': ['powershell']
}


for name, languages in favorite_languages.items():
    print('\n' + name.title() + "'s favorite languages are:")
    for language in languages:
        print("\t" + language.title())

